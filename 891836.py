#!/usr/bin/env python3
import functools
def printItems(di:dict):
    for it in di.keys():
        print(it)
        
f = open('input.txt')
items = dict()
for line in f.readlines():
    items[line.rstrip()] = 0
students = dict()

student_n = input()
while(student_n != ""):
    students[student_n] = []
    printItems(items)
    item_input = input()
    while(item_input != ""):
        if (item_input in items):
            items[item_input] += 1
            students[student_n].append(item_input)
        else:
            print('Такого предмета нету, попробуйте снова')
        item_input = input()
    student_n = input()
max_vote = {x: y for x, y in filter(lambda x: items[x[0]] == max(items.values()), items.items())}
zero_votes = {x: y for x, y in filter(lambda x: x[1] == 0, items.items())}
one_vote = {x: y for x, y in filter(lambda x: x[1] == 1, items.items())}
all_votes = []
index = 0
for st in students.values():
    if (index == 0):
        all_votes = st
    else:
        all_votes = list(set(all_votes) & set(st))
    index += 1

print('all_votes', all_votes)
print(items)
print(students)
print(max_vote)
print(zero_votes)
print(one_vote)

    
