#!/usr/bin/env python3
def read():
    res = input()
    try:
        res = int(res)
        if (res < 0):
            exit()
    except ValueError:
        print(e)
    return res
try:
    count_memb = read()
    if (count_memb < 3 or count_memb > 105):
        exit()
    arr_res = input().split()
    winer = 0
    for x in arr_res:
        x = int(x)
        if (x < 0 or x > 1000):
            exit()
        if (x > winer):
            winer = x
    gues_place = 0
    cur_res = 0
    for i in range(0, len(arr_res)):
        last_num = int(str(arr_res[i])[-1])
        isset_winner_befoer = (str(winer) in arr_res[:i]) or i == 0
        if (last_num == 5):
            if (isset_winner_befoer \
                and int(arr_res[i]) != cur_res):
                if (i != len(arr_res) - 1):
                    if (int(arr_res[i]) >= int(arr_res[i + 1])):
                        gues_place = i + 1
                        cur_res = int(arr_res[i])
                else:
                    gues_place = i + 1
                    cur_res = int(arr_res[i])
    print(gues_place)
except ValueError as e:
    print(e)