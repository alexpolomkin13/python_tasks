#!/usr/bin/env python3
try:
    str_num = input()
    str_list = str_num.split()
    new_str = ""
    count_zero = 0
    for num in str_list:
        if (num != "0"):
            new_str += num + " "
        else:
            count_zero += 1

    print((new_str + "0 " * count_zero).strip())
except ValueError as e:
    print(e)