#!/usr/bin/env python3
def reverse():
    x = int(input())
    if x != 0:
        reverse()
    print(x)
try:
    reverse()
except ValueError as e:
    print(e)