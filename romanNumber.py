#!/usr/bin/env python3
try:
	num = int(input())
	result_pr = ""
	roman_list = {'I': 1, 'V': 5, 'X' : 10, 'L' : 50, 'C' : 100, 'D' : 500}
	roman_list = sorted(roman_list.items(), key = lambda x : x[1], reverse = True)
	while (num >  0):
		for x in roman_list:
			if (num >= x[1]):
				num = num -  x[1]
				result_pr += x[0]
				break
	print(result_pr)
except ValueError as e:
	raise e