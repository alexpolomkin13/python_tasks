#!/usr/bin/env python3
import math
try:
    num = int(input())
    if (num >= 10):
        max_count_in_group = 6
        count_group = math.ceil(num / max_count_in_group)
        list_group = [0] * count_group
        while num != 0:
            for x in range(0, len(list_group)):
                if (num > 0):
                    list_group[x] += 1
                    num -= 1
        list_group.sort()
        str_res = ""
        for group in list_group:
            str_res += str(group) + "+"
        print(str_res[:-1])
except ValueError as e:
    print(e)