#!/usr/bin/env python3
def is_palindrome(string):
    reversed_string = string[::-1]
    return string == reversed_string
try:
    k = int(input())
    if k >= 1 and k <= 100000:
        count = 0
        while k > 0:
            if k < 10:
                count += 1
            elif is_palindrome(str(k)):
                count += 1
            k -= 1
        print(count)
except ValueError:
    print("Введите число")