#!/usr/bin/env python3
x1 = 0
y1 = 0
x2 = 0
y2 = 0
while True:
    try:
        x1 = int(input("Пожалуйста, укажите x первой шашки: "))
        y1 = int(input("Пожалуйста, укажите y первой шашки: "))
        x2 = int(input("Пожалуйста, укажите x второй шашки: "))
        y2 = int(input("Пожалуйста, укажите y второй шашки: "))
        if (x1 * y1 > 0 and x2 * y2 > 0):
            break
    except ValueError:
        print('Пожалуйста введите целое число')

if (y1 < y2):
    if (x1 + y1 + x2 + y2) % 2 == 0:
        if abs(x2 -x1) > y2 - y1:
            print("NO2")
        else:
            print("YES2")
    else:
        print ("NO3")
else:
    print("NO4")