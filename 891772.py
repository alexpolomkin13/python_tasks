#!/usr/bin/env python3
try:
    str_inp_1 = input().split()
    str_inp_2 = input().split()
    myunion = set(str_inp_1) & set(str_inp_2)
    print(' '.join(myunion))
except ValueError as e:
    print(e)