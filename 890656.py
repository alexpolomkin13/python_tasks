#!/usr/bin/env python3
import math
def read():
    res = input()

    try:
        res = int(res)
    except ValueError as e:
        print(e)

    if res > 1000 or res < 0:
        exit()

    return res
try:
    max_1 = read()
    max_2 = read()
    max_3 = read()
    count_table = math.ceil(max_1 / 2) + math.ceil(max_2 / 2) + math.ceil(max_3 / 2)
    print(count_table)
except ValueError as e:
    print(e)