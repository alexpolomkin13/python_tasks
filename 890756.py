#!/usr/bin/env python3
try:
    numb_list = []
    while True:
        numb = int(input())
        if (numb == 0):
            break
        numb_list.append(numb)
    index_max = numb_list.index(max(numb_list))
    numb_list.pop(index_max)
    max_num = max(numb_list)
    print(max_num)
except ValueError as e:
    print(e)