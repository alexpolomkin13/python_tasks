#!/usr/bin/env python3
try:
    str_inp = input().split()
    str_inp = [int(x) for x in str_inp]
    B = sorted(str_inp[:2])
    C = sorted(str_inp[2:])
    B_list = list(range(int(B[0]), int(B[1]) + 1))
    C_list = list(range(int(C[0]), int(C[1]) + 1))
    print(len(set(B_list) & set(C_list)))
except ValueError as e:
    print(e)