#!/usr/bin/env python3
try:
    n = int(input())
    factorial = 1
    if n > 0:
        sumF = 0
        for x in range(1, n + 1):
            print(x)
            factorial *= x
            sumF += factorial
        print(sumF)
    else:
        print(factorial)
except ValueError as e:
    print(e)