#!/usr/bin/env python3
import re
f = open("input.txt", "r", encoding="utf-8")
match_list = []
for line in f:
    match_in_line = re.findall(r'\W?([Вв]рем(?:я|ени|енем))\W?', line)
    match_list += match_in_line
print('\n'.join(match_list))
f.close()