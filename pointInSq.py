#!/usr/bin/env python3
def isPointInSquare(x:float, y:float)->bool:
    x_l, x_r = -1, 1
    y_u, y_d = 1, -1
    x_in_sq = x >= x_l and x <= x_r
    y_in_sq = y <= y_u and y >= y_d

    sum_x_y = abs(x) + abs(y) <= 1

    return x_in_sq and y_in_sq and sum_x_y
try:
    p_x, p_y = float(input()), float(input())
    print("YES" if isPointInSquare(p_x, p_y) else "NO")
except ValueError as e:
    print(e)