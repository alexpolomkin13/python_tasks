#!/usr/bin/env python3
try:
    count_p = int(input())
    children = []
    parents = []
    c_p = []
    for i in range(0, count_p - 1):
        l_str = input().split()
        c_p.append(l_str)
        children.append(l_str[0])
        parents.append(l_str[1])
    list_diff = list(set(parents)-set(children))
    res_list = dict()
    pro_parent = list_diff[0]
    res_list[pro_parent] = 0
    temp_l = [pro_parent]
    level = 1
    while (True):
        temp_l = [it for it in c_p if it[1] in temp_l]
        print(temp_l)
        if (len(temp_l) == 0):
            break
        for te in temp_l:
            res_list[te[0]] = level
        temp = temp_l
        temp_l = []
        for t in temp:
            temp_l.append(t[0])

        level += 1
    res_list = sorted(res_list.items() ,  key=lambda x: x[0])
    for it in res_list:
        print(it[0] + " " + str(it[1]))
except ValueError as e:
    print(e)