#!/usr/bin/env python3
try:
    str_num = input()
    str_list = str_num.split()
    count = 0
    for i in range(0, len(str_list)):
        for j in range(i + 1, len(str_list)):
            if (str_list[i] == str_list[j]):
                count += 1
    print(count)
except ValueError as e:
    print(e)