#!/usr/bin/env python3
from functools import reduce
def numWhMod(num:int, power:int):
    res = num - (num // power) * power
    return res
def bezout_recursive(a, b):
    '''A recursive implementation of extended Euclidean algorithm.
    Returns integer x, y and gcd(a, b) for Bezout equation:
        ax + by = gcd(a, b).
    '''
    if not b:
        return (1, 0, a)
    y, x, g = bezout_recursive(b, a % b)
    return (x, y - (a // b) * x, g)
#const Брать отсюда https://mairu.sharepoint.com/:w:/r/sites/121_12220/_layouts/15/Doc.aspx?sourcedoc=%7B20FA62B4-F396-4CA5-B6CD-51CDFD39764D%7D&file=%D1%82%D0%B0%D0%B1%D0%BB%D0%A0%D0%B0%D1%81%D1%87%D0%97%D0%B0%D0%B43%D0%BC%D0%BE%D0%B4%D0%90%D1%80%D0%B8%D1%84%D0%BC.docx&action=default&mobileredirect=true&cid=68a56c34-68c1-445c-8c2a-61c650960a2e
N = 9
P = [2293, 2297, 2309, 2311, 2333, 1153,1163]
A = [806, 540, 2147, 2046, 1199]
B = [2145, 1827, 969, 840, 2018]
#

P_big = int(reduce((lambda x, y: x * y), P[:5]))
m_dict = dict()
for i in range(0, len(P[:5])):
    num = bezout_recursive(P_big / P[i], P[i])[0]
    if (num < 0 ):
        num = numWhMod(num, P[i])
    m_dict['m' + str(i + 1)] = int(num)
print(m_dict)
Z_a = 0
Z_b = 0
for i in range(0, len(A)):
    Z_a += ((A[i] * m_dict['m' + str(i + 1)]) / P[i])
    Z_b += ((B[i] * m_dict['m' + str(i + 1)]) / P[i])

print('Z_a:', Z_a)
print('Z_b:', Z_b)
A_P_6 = 0
B_P_6 = 0
A_P_7 = 0
B_P_7 = 0
index = 6
for p in P[5:]:
    num_A = 0
    num_B = 0
    for i in range(0 ,5):
        nod = int(bezout_recursive(P[i], p)[0])
        if (nod < 0 ):
            nod = numWhMod(nod, p)
        comm_p = numWhMod(m_dict['m' + str(i + 1)] * P_big, p)
        num_A += ((numWhMod(A[i] * nod, p) * comm_p))
        num_B += ((numWhMod(B[i] * nod, p) * comm_p))
    num_A = numWhMod((num_A - numWhMod(int(Z_a) * P_big, p)), p)
    num_B = numWhMod((num_B - numWhMod(int(Z_b) * P_big, p)), p)

    exec('A_P_%s = num_A' % index)
    exec('B_P_%s = num_B' % index)
    index += 1
print('A_P_6: ', A_P_6)
print('A_P_7: ', A_P_7)
print('B_P_6: ', B_P_6)
print('B_P_7: ', B_P_7)
A.extend([A_P_6, A_P_7])
B.extend([B_P_6, B_P_7])


#Аддитивные и мультипликативные операции с модулярными операндами  А,  В. 
C = [numWhMod(A[i] + B[i], P[i]) for i in range(0, len(A))]
print('С:', C)
E = [numWhMod(A[i] - B[i], P[i]) for i in range(0, len(A))]
print('E:', E)
D = [numWhMod(A[i] * B[i], P[i]) for i in range(0, len(A))]
print('D:', D)
A_2 = [numWhMod(A[i] ** 2, P[i]) for i in range(0, len(A))]
print('A_2:', A_2)
B_2 = [numWhMod(B[i] ** 2, P[i]) for i in range(0, len(A))]
print('B_2:', B_2)