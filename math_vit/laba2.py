#!/usr/bin/env python3
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import numpy 
from matplotlib import cm
import pylab
def numWhMod(num:int, power:int):
    res = num - (num // power) * power
    return res
#const
N = 9
DD = '04'
MM = '03'
GG = 1998
GG_m = 98
M_c_D = MM + DD
D_c_M = DD + MM
DD = int(DD)
MM = int(MM)
A = int(M_c_D) / 12
print('A:', A)
B = int(D_c_M) / 31
print('B:', B)

h_x = DD / GG_m
print('h_x:', h_x)
h_y = MM / GG_m
print('h_y:', h_y)

k_x = numWhMod(11 * N + DD, 7) + 3
print('k_x:', k_x)
k_y = numWhMod(13 * N + MM, 7) + 3
print('k_y:', k_y)
d = numWhMod(7 * N + MM, 5) + 3
print('d:', d)

a = (numWhMod(DD + MM, 11) + int(D_c_M)) / GG
print('a:', a)
b = (numWhMod(DD + MM, 11) + int(M_c_D)) / GG
print('b:', b)
c = (int(D_c_M) + 31) / GG
print('c:', c)

#количество внетренних и граничных точек аппроксимации функции f(x, y)
u = round(A / h_x)
print('u:', u)
v = round(B / h_y)
print('v:', v)

def makeData ():
    x = numpy.arange (-10, 10, 0.1)

    y = numpy.arange (-10, 10, 0.1)
    xgrid, ygrid = numpy.meshgrid(x, y)

    zgrid = numpy.sin (xgrid) * numpy.sin (ygrid) / (xgrid * ygrid)
    return xgrid, ygrid, zgrid

x, y, z = makeData()

fig = pylab.figure()
axes = Axes3D(fig)

axes.plot_surface(x, y, z)

#pylab.show()