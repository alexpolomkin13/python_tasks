#!/usr/bin/env python3
import math
import matplotlib.pyplot as plt

#from sympy import Symbol
def numWhMod(num:int, power:int):
    res = num - (num // power) * power
    return res
def g_n(n:int, p:int):
    res = math.log(p ** 2, 2) / (math.log(p ** 2, 2) + n * abs(math.log(n, 2)) / p)
    return res
def g_n_t(n:int, p:int, t:int):
    res = math.log(p ** 2, 2) / (math.log(p ** 2, 2) + n * abs(math.log(n, 2)) * t / p)
    return res
def s_p_func(a:int, p:int):
    return (1 / (a + (1 - a) / p))
def s_p_k_func(a:int, p:int, c:int):
    return (1 / (a + (1 - a) * t / p + c))
def printDict(dict):
	for key, value in dict.items():
  		print("{0}: {1}".format(key,value))
N = 9 #номер варианта
NOther = 11 + 1 - N #N с чертой
DD = "04"
MM = "03"
GG = "1998"
p = 3 * N + numWhMod(int(DD), 5) #количество процессоров в вычислительном алгоритме
print("P = ", p)
a = 5 * N / (NOther + 5 * N) #дол последовательных операций в вычислительном алгоритме
print("A = ", a)
#Коэффициенты замедления коммуникационной среды равны: 
t = (int(MM + DD) + N) / int(MM + DD)
print("t = ", t)
c = min(NOther, N) / (NOther + N)
print("c = ", c)

k_z = dict()
for i in range(1, p + 1):
    #k_z.append(N / (N + numWhMod(i * (DD + i), 7)))
    k_z[i] = N / (N + numWhMod(i * (int(DD) + i), 7))
print("k_z")
printDict(k_z)

k_p = sum(k_z.values()) / p
print("k_p_z = ", k_p)
#plt.subplot(121)
plt.figure(figsize=(12, 4))
plt.title("коэффициенты загрузки вычислительных узлов")
plt.xlabel("p")
plt.ylabel("k")
plt.plot(k_z.keys(), k_z.values())
plt.plot((0, p + 1), (k_p, k_p))




#коэффициенты ускорения с учетом и без учета влияния коммуникационной среды
s_p = dict()
s_p_k = dict()

for i in range(1, p + 1):
    s_p[i] = s_p_func(a, i)
    s_p_k[i] = s_p_k_func(a, i, c)


print("s_p")
printDict(s_p)

print("s_p_k")
printDict(s_p_k)

plt.figure(figsize=(12, 4))
plt.title("вычислить коэффициенты ускорения с учетом и без учета влияния коммуникационной среды")
plt.xlabel("p")
plt.ylabel("s")
plt.plot(s_p.keys(), s_p.values())
plt.plot(s_p_k.keys(), s_p_k.values())

s_p_n = dict() #коэффициенты ускорения масштаба задачи.
for x in range(1, 21):
    nPx = 2 ** x
    formulaS_P_N = p + (1 - p) * g_n(nPx, p)
    s_p_n[x] = formulaS_P_N
s_p_n_t = dict() #коэффициенты ускорения масштаба задачи.
for x in range(1,21):
    nPx = 2 ** x
    formulaS_P_N_T = p + (1 - p) * g_n_t(nPx, p, t)
    s_p_n_t[x] = formulaS_P_N
print("s_p_n")
printDict(s_p_n)

print("s_p_n_t")
printDict(s_p_n_t)
plt.figure(figsize=(12, 4))
plt.title("коэффициенты ускорения масштаба задачи")
plt.xlabel("p")
plt.ylabel("s")
plt.plot(s_p_n.keys(), s_p_n.values())
plt.plot(s_p_n_t.keys(), s_p_n_t.values())
plt.show()
