#!/usr/bin/env python3
while True:
    try:
        l1 = int(input("Пожалуйста, укажите l первой спички: "))
        r1 = int(input("Пожалуйста, укажите r первой спички: "))
        if not(r1 <= 100 and l1 >= 0 and r1 > l1):
            continue
        l2 = int(input("Пожалуйста, укажите l второй спички: "))
        r2 = int(input("Пожалуйста, укажите r второй спички: "))
        if not(r2 <= 100 and l2 >= 0 and r2 > l2):
            continue
        l3 = int(input("Пожалуйста, укажите l второй спички: "))
        r3 = int(input("Пожалуйста, укажите r второй спички: "))
        if not(r3 <= 100 and l3 >= 0 and r3 > l3):
            continue
        break
    except ValueError:
        print('Пожалуйста введите целое число')
def getSpace(sp_l1, sp_r1, sp_l2, sp_r2):
    space = max(sp_l2, sp_l1) - min(sp_r1, sp_r2)
    if space < 0:
        return 0
    return space
space1 = getSpace(l1, r1, l2, r2)
space2 = getSpace(l2, r2, l3, r3)
space3 = getSpace(l1, r1, l3, r3)
lenl1 = abs(r1 - l1)
lenl2 = abs(r2 - l2)
lenl3 = abs(r3 - l3)
maxSpace = space1 if space1 > space2 else space2
if space1 + space2 == 0 \
        or space2 + space3 == 0 \
        or space1 + space3 == 0:
    print(0)
# elif (maxSpace > lenl1 and maxSpace > lenl2 and maxSpace > lenl3):
#     print(-1)
else:
    if (lenl1 >= space2):
        print(1)
    elif (lenl2 >= space1 and (lenl2 >= space2 or lenl2 >= space3)):
        print(2)
    elif (lenl3 >= space1 or lenl3 >= space2):
        print(3)
    else:
        print(-1)