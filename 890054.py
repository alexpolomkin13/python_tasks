#!/usr/bin/env python3
try:
    str_num = input()
    str_list = str_num.split()
    count = 0
    for i in range(1, len(str_list) - 1):
        num = int(str_list[i])
        if ((num > int(str_list[i - 1])) and (num > int(str_list[i + 1]))):
            count += 1
    print(count)
except ValueError as e:
    print(e)