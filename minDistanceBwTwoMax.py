#!/usr/bin/env python3
x = 1
numbers_list = []
number = 0
print("Пожалуйста введите последовательность натуральных чисел через Enter, завершающейся числом 0")
while x != 0:
    try:
        number = int(input())
        if number < 0:
            print('Пожалуйста введите натуральное число')
            continue
        numbers_list.append(number)
        x = numbers_list[len(numbers_list) - 1]
    except ValueError:
        print('Пожалуйста введите целое число')
print("Ваша последовательность: ",numbers_list ,'\n')

maxx1 = 0
maxx2 = 0
res = -1
for x in range(1, len(numbers_list) - 1):
    if numbers_list[x] > numbers_list[x - 1] and numbers_list[x] > numbers_list[x + 1]:
        maxx2 = maxx1
        maxx1 = x
        if (maxx1 > 0) and (maxx2 > 0) and ((maxx1 - maxx2 < res) or res < 0):
            res = maxx1 - maxx2
print("Наименьшее расстояние:", res)