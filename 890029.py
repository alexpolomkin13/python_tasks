#!/usr/bin/env python3
try:
    a, b, c = int(input()), int(input()), int(input())
    d, e = int(input()), int(input())
    count = 0
    for x in range(0, 1001):
        if (x - e == 0):
            continue
        sol = (a * x ** 3 + b * x ** 2 + c * x + d) / (x - e)
        if (sol == 0):
            count += 1
    print(count)
except ValueError as e:
    print(e)