#!/usr/bin/env python3
import sys
import math
str_inp = sys.stdin.readlines()
parties = dict()
count_common = 0
count_dep = 450
for s in str_inp:
    s = s.strip()
    if (s == ""):
        break
    s = s.split()
    count_votes = int(s[-1])
    count_common += count_votes
    name_party = ' '.join(s[0: len(s) - 1])
    if (name_party in parties):
        parties[name_party] += count_votes
    else:
        parties[name_party] = count_votes
first_quotient = math.ceil(count_common / count_dep)
parties_votes = dict()
for part in parties.items():
    vote = round(part[1] / first_quotient)
    parties_votes[part[0]] = vote
    count_dep -= vote

if (count_dep != 0):
    parties_sort = sorted(parties_votes.items() ,  key = lambda x: x[1])

    last_vote = count_dep // len(parties_votes.keys())
    for pa in parties_sort:
        if (count_dep == 0):
            break
        parties_votes[pa[0]] += last_vote
        count_dep -= last_vote

    while (count_dep != 0):
        for p in parties_sort:
            if (count_dep == 0):
                break
            parties_votes[p[0]] += 1
            count_dep -= 1
for it in parties_votes.items():
    print(it[0] + " " +  str(it[1]))