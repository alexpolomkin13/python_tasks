#!/usr/bin/env python3
def reformat(telephone):
    telephone = telephone.replace('-', '').replace('(', '').replace(')', '')
    return telephone[-10:] if len(telephone) > 7 else '495' + telephone[-7:]
n = 4
notes = [input() for _ in range(n)]
for note in notes[1:]:
    print('YES' if reformat(notes[0]) == reformat(note) else 'NO')
