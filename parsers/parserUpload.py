#!/usr/bin/env python3
import requests
from bs4 import BeautifulSoup
import urllib.request
import os
import os.path
import urllib3
def getContentFrUrl(url):
    response = requests.get(url)
    if (response.status_code == 200):
        return response.text
    else:
        return ""
def check404(url):
    http = urllib3.PoolManager()
    error = http.request("GET", url)
    return (error.status == 404)

project_name = 'mvd_stand'
             
path_save = "/home/alex/PhpstormProjects/" + project_name + "/"

ulr_for_parser = input("Введите url для парсинга: ")
content = getContentFrUrl(ulr_for_parser)
if (content != ""):
    soup = BeautifulSoup (content, 'lxml')
    for link in soup.find_all('img'):
        srcHref = str(link.get('src'))
        indexLastSlah = str.rfind(srcHref, '/')
        indexUpl = str.find(srcHref,'upload')
        full_path = path_save + srcHref[indexUpl: indexLastSlah]
        if (indexUpl > 0):
            if (not(check404("https:" + srcHref))):
                resource = urllib.request.urlopen("https:" + srcHref)
                os.makedirs(full_path, exist_ok=True)
                if (not(os.path.exists(path_save + srcHref[indexUpl:]))):
                    print("Downloading  " + path_save + srcHref[indexUpl:])
                    out = open(path_save + srcHref[indexUpl:], 'wb')
                    out.write(resource.read())
                    out.close()
            else:
                print("https:" + srcHref + " not found")
    

