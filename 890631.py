#!/usr/bin/env python3
str_inp = input()
if str_inp.count('h') < 2:
    exit()
start_index_str = str_inp.index('h') + 1
end_index_str = len(str_inp) - 1 - str_inp[::-1].index('h')
str_res = str_inp[0: start_index_str] + str_inp[start_index_str:end_index_str][::-1] \
        + str_inp[end_index_str:len(str_inp)]
print(str_res)