#!/usr/bin/env python3
try:
    num = int(input())
    if num == 0:
        print(0)
    else:
        l_fib, r_fib = 0, 1
        n = 1
        while r_fib <= num:
            if r_fib == num:
                print(n)
                break
            l_fib, r_fib = r_fib, l_fib + r_fib
            n += 1
        else:
            print(-1)
except ValueError as e:
    print(e)