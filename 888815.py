#!/usr/bin/env python3
def isPointInSquare(x:float, y:float)->bool:
    xy_in_c = (x + 1) ** 2 + (y - 1) ** 2 <= 4
    xy_above = ((y >= 0) and (y + x >= 0) and (y - 2 * x >= 2))
    xy_below = ((y <= 0) and (y + x <= 0) and (y - 2 * x <= 2))
    return (xy_in_c and xy_above) or (not(xy_in_c) and xy_below)
try:
    p_x, p_y = float(input()), float(input())
    print("YES" if isPointInSquare(p_x, p_y) else "NO")
except ValueError as e:
    print(e)