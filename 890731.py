#!/usr/bin/env python3
try:
    x = int(input())
    y = int(input())
    index = 1
    if x <= 0:
        print(0)
        exit()
    while x < y:
        x *= 1.1
        index += 1
    print(index)
except ValueError as e:
    print(e)