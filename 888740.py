#!/usr/bin/env python3
from math import sqrt
def listToString(s):
    str1 = ""
    for ele in s:
        str1 += str(ele) + " "
    return str1
try:
    result = []
    while True:
        n = int(input())
        if n == 0:
            break
        if n > 0:
            root = sqrt(n)
            if root % 1 == 0:
                result.append(n)
    result.reverse()
    listStr = listToString(result)
    if listStr == "":
        print(0)
    else:
        print(listStr)
except ValueError as e:
    print(e)