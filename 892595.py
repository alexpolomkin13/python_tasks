#!/usr/bin/env python3
def read():
    res = input()
    return res
try:
    all_parties = dict()
    str_inp = " "
    votes = False
    while str_inp != "":
        str_inp = read()
        if (str_inp != "PARTIES:"):
            if (str_inp == "VOTES:"):
                votes = True
            if (not(votes)):
                all_parties[str_inp] = 0
            else:
                if (str_inp in all_parties):
                    all_parties[str_inp] += 1
    sort_alf = sorted(all_parties.items(), key = lambda x:x[0])
    sort_dic = sorted(sort_alf, key = lambda x:x[1],reverse = True)
    for it in sort_dic:
        print(it[0])
except ValueError as e:
    print(e)