#!/usr/bin/env python3
try:
    str_i = input().split()
    c_days = int(str_i[0])
    c_part = int(str_i[1])
    days_z = []
    for i in range(0, c_part):
        s = input().split()
        num_p = int(s[0])
        period = int(s[1])
        res = num_p
        while (res <= c_days):
            days_z.append(res)
            res += period
    days_filter = list(filter(lambda x: ((x + 1) % 7 != 0) and (x % 7 != 0), days_z))
    days_filter = set(days_filter)
    print(len(days_filter))
except ValueError as e:
    print(e)