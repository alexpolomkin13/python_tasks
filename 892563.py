#!/usr/bin/env python3
try:
    num = int(input())
    str_res = ""
    res = 0
    if (num == 1):
        print(0)
    elif (num == 2):
        str_res += '1*2=2'
    else:
        for x in range(1, num):
            res += (x * (x + 1))
            str_res += str(x) + "*" + str(x + 1)
            if (x != num - 1):
                str_res += "+"
        str_res += "=" + str(res)
    print(str_res)
except ValueError as e:
    print(e)