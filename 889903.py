#!/usr/bin/env python3
import sys
try:
    n = int(input("Введите число поселений (1 <= n <= 100000): "))
    if (n < 1 and n > 100000):
        sys.exit("1 <= n <= 100000")
    print("Введите расстояние для послеений от начала до 109 для каждого n")
    n_distance = input().split()
    if (n == len(n_distance)):
        for num in n_distance:
            if (int(num) < 0 and int(num) > 109):
                sys.exit("0 <= dist <= 109")
    m = int(input("Введите число убежищ (1 <= n <= 100000): "))
    if (m < 1 and m > 100000):
        sys.exit("1 <= m <= 100000")
    print("Введите расстояние для убежищ от начала до 109")
    m_distance = input().split()
    if (m == len(m_distance)):
        for num in m_distance:
            if (int(num) < 0 and int(num) > 109):
                sys.exit("0 <= dist <= 109")
    result = ""
    for distance_n in n_distance:
        distance_n = int(distance_n)
        min_distance = 0
        index = 1
        min_index = 0
        for distance_m in m_distance:
            distance_m = int(distance_m)
            if (distance_n == distance_m):
                min_index = index
                break
            if (min_distance == 0 or (abs(distance_n - distance_m) < min_distance)):
                min_distance = abs(distance_n - distance_m)
                min_index = index
            index += 1
        result += str(min_index) + " "
    print(result)
except ValueError as e:
    print(e)