#!/usr/bin/env python3

try:
    x1 = int(input("Enter x1: "))
    y1 = int(input("Enter y1: "))
    x2 = int(input("Enter x2: "))
    y2 = int(input("Enter y2: "))
    if not(x1 >= 0 and y1 >= 0 \
        and x2 >= 0 and y2 >= 0):
        print ("NO")
    else:
        if (abs(y1 - y2) <= 1) and (abs(x1 - x2) <= 1):
            print("YES")
        else:
            print("NO")
except ValueError:
    print('Пожалуйста введите целое число')