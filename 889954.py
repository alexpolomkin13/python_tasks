#!/usr/bin/env python3
try:
    f = open('input.txt', 'r', encoding='utf-8')
    result = dict()
    for line in f:
        school_list = line.split()
        if (school_list[2] in result):
            result[school_list[2]]['count'] += 1
            result[school_list[2]]['balls'] += int(school_list[3])
        else:
            result[school_list[2]] = dict()
            result[school_list[2]]['count'] = 1
            result[school_list[2]]['balls'] = int(school_list[3])
    f.close()
    max_count = 0
    for x in list(result.items()):
        if (max_count == 0 or x[1]['count'] > max_count):
            max_count = x[1]['count']
    filt_res = list(filter(lambda x: x[1]['count'] == max_count, result.items()))
    sort_res = sorted(filt_res, key=lambda x: x[1]['balls'], reverse = True)
    str_res = ""
    for x in sort_res:
        str_res += str(x[0]) + " "
    print(str_res)
except ValueError as e:
    print(e)