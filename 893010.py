#!/usr/bin/env python3
f = open('input.txt')
words_d = dict()
str_res = ""
for line in f.readlines():
    str_s = line.split()
    for s in str_s:
        if (not(s in words_d)):
            words_d[s] = 0
        else:
            words_d[s] += 1
        str_res += str(words_d[s]) + " "
print(str_res)