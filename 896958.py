#!/usr/bin/env python3
import sys
print(any(map(lambda x: int(x.strip()) == 0, sys.stdin.readlines())))