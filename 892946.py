#!/usr/bin/env python3
f = open('input.txt')
words_d = dict()
str_res = ""
for line in f.readlines():
    str_s = line.split()
    for s in str_s:
        if (not(s in words_d)):
            words_d[s] = 1
        else:
            words_d[s] += 1
sort_words = dict(sorted(words_d.items() ,  key=lambda x: x[0]))
sort_words = dict(sorted(sort_words.items() ,  key=lambda x: x[1], reverse = True))
for item in sort_words.keys():
    print(item)