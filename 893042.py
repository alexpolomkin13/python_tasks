#!/usr/bin/env python3
def get_key(d, value):
    for k, v in d.items():
        if v == value:
            return k
try:
    count_words = int(input())
    dict_words = dict()
    for x in range(0, count_words):
        word = input()
        dict_words[word] = word.lower()
    str_text = input().split()
    count_wrong = 0
    for s in str_text:
        s_l = s.lower()
        if (s == s_l):
            count_wrong += 1
            continue
        letters_upper = list(filter(lambda x: x == x.upper(), list(s)))
        if (len(letters_upper) > 1):
            count_wrong += 1
            continue
        if (get_key(dict_words, s_l) is None):
            continue
        if (not(s in dict_words)):
            count_wrong += 1
    print(count_wrong)
except ValueError as e:
    print(e)