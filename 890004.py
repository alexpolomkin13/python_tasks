#!/usr/bin/env python3
def CountSort(A:list)->list:
    max_num = max(A) + 1
    letter_frequency = [0] * max_num
    for x in A:
        letter_frequency[x] += 1
    A = []
    for number in range(max_num):
        A += [number] * letter_frequency[number]
    return A
try:
    num_list = input().split()
    num_list = [int(item) for item in num_list]
    sort_list = CountSort(num_list)
    for num in sort_list:
        print(num, end=" ")
except ValueError as e:
    print(e)