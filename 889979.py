#!/usr/bin/env python3
try:
    classes = dict()
    f = open('input.txt', 'r', encoding='utf-8')
    for line in f:
        classes_list = line.split()
        classes_list[2] = int(classes_list[2])
        classes_list[3] = int(classes_list[3])
        if ((classes_list[2] in classes) and (classes[classes_list[2]]['ball'] < classes_list[3])):
            classes[classes_list[2]]['ball'] = classes_list[3]
        elif(not(classes_list[2] in classes)):
            classes[classes_list[2]] = dict()
            classes[classes_list[2]]['ball'] = classes_list[3]
    f.close()
    sort_classes = sorted(classes.items(), key = lambda x: x[0])
    str_res = ""
    for item in sort_classes:
        str_res += str(item[1]['ball']) + " "
    print(str_res)
except ValueError as e:
    print(e)