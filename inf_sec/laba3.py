#!/usr/bin/env python3
import math
def numWhMod(num:int, power:int):
    res = num - (num // power) * power
    return res
def bezout_recursive(a, b):
    '''A recursive implementation of extended Euclidean algorithm.
    Returns integer x, y and gcd(a, b) for Bezout equation:
        ax + by = gcd(a, b).
    '''
    if not b:
        return (1, 0, a)
    y, x, g = bezout_recursive(b, a % b)
    return (x, y - (a // b) * x, g)
#CONST
N = 9	
DD = '04'
MM = '03'
GG = '1998'
p = 2411
g = 1009
n = numWhMod(N, 3) + 7
print('n:', n)
c = int('21' + MM + '22' + DD)
print('c:', c)
A = [int('21' + MM), int('22' + DD)]
B = [int('22' + DD), int('21' + MM)]
for i in range(2, n):
    if (i % 2 == 0):
        A.append(numWhMod(A[i - 1] ** 2, p))
        B.append(numWhMod(B[i - 1] ** 2, p))
    else:
        A.append(numWhMod(B[i - 1], p))
        B.append(numWhMod(A[i - 1], p))
print('A:', A)
print('B:', B)
sum_a = 0
sum_b = 0
for i in range(0, len(A)):
    if (i % 2 == 0):
    	sum_a += A[i]
    	sum_b += B[i]

sum_a = numWhMod(sum_a, g)
sum_b = numWhMod(sum_b, g)
print('sum_a:', sum_a,'sum_b:', sum_b)
W = (math.log2(sum_a) + math.log2(sum_b)) / (math.log2(A[0]) + math.log2(B[0])) 
print('W:', W)