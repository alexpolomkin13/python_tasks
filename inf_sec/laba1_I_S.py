#!/usr/bin/env python3
def numWhMod(num:int, power:int)->int:
    res = num - (num // power) * power
    return res
def processEnc(alfIndex:int , k:int , b:int, c:int)->int:
    res = numWhMod(numWhMod(k * alfIndex + b, 31) + c, 31) + 1
    return res
def processDecr(k:int , b:int , c:int, beta:int)->int:
    res = numWhMod(numWhMod(numWhMod(beta - c - 1, 31) - b, 31) * k, 31)
    return res
def convert_list_to_string(org_list, seperator=' '):
    """ Convert list to string, by joining all item in list with given separator.
        Returns the concatenated string """
    return seperator.join(org_list)
def buildTable(l:list, step:int):
    for i in range(0, step):
        index = i
        while index < len(l):
            print(str(l[index]) + '\t', end="")
            index += step
        print("")
N = 9
DD = "04"
MM = "03"
GG = "1998"
rus_alpha = ["А", "Б" , "В", "Г", "Д" , "Е", "Ж", "З" , "И", "Й", "К" , "Л", "М", "Н" , "О", "П", "Р" , "С", "Т", "У" , "Ф", "Х", "Ц" , "Ч", "Ш", "Щ" , "Ы", "Ь", "Э" , "Ю", "Я"]
k = N + 3
b = int(MM)
c = int(DD)
k_const = 13
str_input = "ПРОЕКТИРОВЩИКПОЛОМКИНАЛЕКСЕЙАЛЕКСАНДРОВИЧАВТОМАТЧИК"
enc_list = []
dec_list = []
output_list = []
if (numWhMod(k * k_const, 31) == 1):
    try:
        #процесс шифрования
        index = 1
        for ch in str_input:
            output_list.append(index)
            output_list.append(ch)
            alf_index = rus_alpha.index(ch) + 1
            output_list.append(alf_index)

            b_encry = processEnc(alf_index, k, b, c)
            output_list.append(b_encry)

            enc_list.append(rus_alpha[b_encry - 1])
            output_list.append(rus_alpha[b_encry - 1])

            a_decry = processDecr(k_const, b, c, b_encry)
            output_list.append(a_decry)

            dec_list.append(rus_alpha[a_decry - 1])
            output_list.append(rus_alpha[a_decry - 1])
            index += 1
        #процесс дешифрования
        buildTable(output_list, 7)
        print(numWhMod(51, 31))
    except ValueError as e:
        print(e)