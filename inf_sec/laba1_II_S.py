#!/usr/bin/env python3
def numWhMod(num:int, power:int)->int:
    res = num - (num // power) * power
    return res

def wordShift(word:str, shift:int, alf:list, direction = "right")->str:
    str_res = ""
    for w in word:
        ind = alf.index(w) + 1
        if (direction == 'right'):
            indWShift = (ind + shift) % len(alf)
        else:
            indWShift = (ind - shift) % len(alf)
        str_res += alf[indWShift]
    return str_res
def find_v(a:int, s:int)->int:
    res = numWhMod(a + s, 31)
    return res

def processEnc(k:int , v:int , b:int, c:int)->int:
    res = numWhMod(numWhMod(k * v + b, 31) + c, 31) + 1
    return res
def u_i(k:int , b:int , c:int, beta:int)->int:
    res = numWhMod(numWhMod(numWhMod(beta - c - 1, 31) - b, 31) * k, 31)
    return res
def processDecr(u:int, s:int)->int:
    res = numWhMod(u - s, 31)
    return res
def convert_list_to_string(org_list, seperator=' '):
    """ Convert list to string, by joining all item in list with given separator.
        Returns the concatenated string """
    return seperator.join(org_list)
def buildTable(l:list, step:int):
    f = open('resultLab.txt', 'w')
    for i in range(0, step):
        index = i
        while index < len(l):
            f.write(str(l[index]) + '\t')
            index += step
        f.write("")
def text_to_bits(text, encoding='utf-8', errors='surrogatepass'):
    bits = bin(int.from_bytes(text.encode(encoding, errors), 'big'))[2:]
    print(bits)
    return bits.zfill(8 * ((len(bits) + 7) // 8))



N = 9
DD = 4
MM = 3
GG = 1998
b = MM
c = DD
rus_alpha = ["А", "Б" , "В", "Г", "Д" , "Е", "Ж", "З" , "И", "Й", "К" , "Л", "М", "Н" , "О", "П", "Р" , "С", "Т", "У" , "Ф", "Х", "Ц" , "Ч", "Ш", "Щ" , "Ы", "Ь", "Э" , "Ю", "Я"]
k = N + 3
k_const = 13
str_input = "РАЗРАБОТЧИКПОЛОМКИНАЛЕКСЕЙАЛЕКСАНДРОВИЧСИСТЕМЩИК"

stix="НОЧЬУЛИЦАФОНАРЬАПТЕКАБЕССМЫСЛЕННЫЙИТУСКЛЫЙСВЕТЖИВИ"
str_shift = wordShift(str_input, N, rus_alpha)
output_list = []
enc_list = []
dec_list = []
if (numWhMod(k * k_const, 31) == 1):
    try:
        #процесс шифрования
        index = 0
        for i in range(0, len(str_shift)):
            #добавляем в список итый элемент сдвинутой строки, сам индекс
            output_list.append(index + 1)
            output_list.append(stix[i])
            
            #получаем индекс итого смивола стиха в алфавите
            s_index_in_alpha = rus_alpha.index(stix[i]) + 1
            #получаем индекс итого смивола исходной строки в алфавите
            input_index_in_alpha = rus_alpha.index(str_shift[i]) + 1
            output_list.append(s_index_in_alpha)
            output_list.append(str_input[i])
            output_list.append(str_shift[i])

            output_list.append(input_index_in_alpha)
            #выполняем  процедуру шифрования
            v = find_v(s_index_in_alpha, input_index_in_alpha)
            output_list.append(v)
            b_encry = processEnc(k, v, b, c)

            #добавляем в список букву
            enc_list.append(rus_alpha[b_encry - 1])
            output_list.append(b_encry)

            output_list.append(rus_alpha[b_encry - 1])

            #ВСТАВИТЬ ДВОИЧНОЕ ПРЕДСТАВЛЕНИЕ


            #процесс дешифрования
            u = u_i(k_const,b, c, b_encry)
            output_list.append("D")
            output_list.append(u)
            y_decry = processDecr(u, s_index_in_alpha)
            output_list.append(y_decry)

            dec_list.append(rus_alpha[y_decry - 1])
            output_list.append(rus_alpha[y_decry - 1])
            output_list.append(str_input[i])
            index += 1
        
        buildTable(output_list, 14)
        print("Исходная строка", str_input)
        print("Сдвинутая строка", convert_list_to_string(str_shift))
        print("Зашифрованная строка", convert_list_to_string(enc_list))
        print("Расшифрованная строка", convert_list_to_string(dec_list))
        print(numWhMod(48, 31))
    except ValueError as e:
        print(e)