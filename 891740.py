#!/usr/bin/env python3
import sys
import re
def Union(lst1, lst2):
    final_list = lst1 + lst2
    return final_list

list_words = []
for line in sys.stdin:
    #line = re.sub(r"[#%!@:;,.*]", "", line)
    #line_s = [s.lower() for s in line.split()]
    list_words = Union(list_words, line.split())
print(len(set(list_words)))
