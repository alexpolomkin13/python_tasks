#!/usr/bin/env python3
import sys
import math
def checkAccount(accounts:dict, account:str):
    return (account in accounts)

def createAccount(accounts:dict, name:str, start_sum:int = 0):
    if (not(checkAccount(accounts, name))):
        accounts[name] = start_sum

def checkBalance(accounts:dict, name:str):
    if (checkAccount(accounts, name)):
        return accounts[name]
    else:
        return "ERROR"

def withdraw(accounts:dict, name:str, money:int):
    if (checkAccount(accounts, name)):
        accounts[name] -= money
    else:
        createAccount(accounts, name, money * (-1))

def transfer(accounts:dict, n_from:str, n_to:str, money:int):
    createAccount(accounts, n_from)
    createAccount(accounts,n_to)
    accounts[n_from] -= money
    accounts[n_to] += money


def income(accounts:dict, p:int):
    for client in accounts.items():
        if (client[1] > 0 ):
            accounts[client[0]] = client[1] + math.floor(client[1] * p / 100)

def deposit(accounts:dict, account:str, money:int):
    if (checkAccount(accounts, account)):
        accounts[account] += money
    else:
        createAccount(accounts, account, money)

def get_action(str_l:list, accounts:dict):
    action = str_l[0]
    if (action == 'DEPOSIT') :
        deposit(accounts, str_l[1], int(str_l[2]))
    elif (action == 'INCOME') :
        income(accounts, int(str_l[1]))
    elif (action == "BALANCE") :
        return (checkBalance(accounts, str_l[1]))
    elif (action == 'WITHDRAW') :
        withdraw(accounts, str_l[1], int(str_l[2]))
    elif (action == 'TRANSFER') :
        transfer(accounts, str_l[1], str_l[2], int(str_l[3]))
accounts_users = dict()
try:
    res_str = ""
    str_inp = sys.stdin.readlines()
    for s in str_inp:
        s = s.strip()
        if (s == ""):
            exit()
        else:
            s = s.split()
            temp = get_action(s, accounts_users)
            if (not(temp is None)):
                res_str += str(temp) + '\n'
    print(res_str)
except ValueError as e:
    print(e)