#!/usr/bin/env python3
try:
    num = int(input("Enter number1: "))
    num2 = int(input("Enter number2 (number 2 < number 1: "))
    if num * num2 > 0 and num > num2:
        while num != num2:
            if (num % 2 == 0) and (num / 2 > num2):
                num = num / 2
                print(":2")
            elif (num - 1 >= num2):
                num = num - 1
                print("-1")
except ValueError:
    print('Пожалуйста введите целое число')