#!/usr/bin/env python3
speed = 0
t = 0
while True:
    try:
        speed = int(input("Пожалуйста, укажите скорость Васи в диапазоне от -1000 до 1000: "))
        t = int(input("Пожалуйста, укажите время, оно должно быть больше 0: "))
        if (speed > -1001 and speed < 1001 and t > 0):
            break
        print("Укажите скорость в диапазон от -1000 до 1000 и/или проверьте время")
    except ValueError:
        print('Пожалуйста введите целое число')

print(speed * t % 109)