#!/usr/bin/env python3
import re
def f_2_celsia(num):
    num = int(num)
    res = (num - 32) / 1.8
    return str(res)
str_i = input()
print(re.sub(r'(\d+)F', lambda m: f_2_celsia(m.group(1)), str_i))