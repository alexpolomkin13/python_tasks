#!/usr/bin/env python3
def gcd(a , b):
    ost = a % b
    if (ost == 0):
        return b
    else:
        return gcd(b, ost)
try:
    num_1 = int(input())
    num_2 = int(input())
    print(gcd(num_1, num_2))
except ValueError as e:
    print(e)