#!/usr/bin/env python3
import numpy as np
def matmultiply(a, b):
    res = np.zeros(a.shape, dtype=int)
    size = a.shape[0]
    for i in range(size):
        for j in range(size):
            for k in range(size):
                res[i][j] += a[i][k] * b[k][j]
    return res
class Matrix:
    def __init__(self, matr:list):
        self.matr_list = matr
    def __str__(self):
        return (('\n'.join(map(lambda x: ('\t'.join(map(str, x))).strip(),\
            self.matr_list))).strip())
    def size(self):
        return (len(self.matr_list), len(self.matr_list[0]))
    def __add__(self, add_matr):
        temp = list()
        for i in range(0, len(self.matr_list)):
            temp.append([sum(z) for z in zip(self.matr_list[i], add_matr.matr_list[i])])
        return Matrix(temp)
    def __mul__(self, scolar:int):
        temp = list()
        for i in range(0, len(self.matr_list)):
            temp.append(map(lambda x: x * scolar, self.matr_list[i]))
        return Matrix(temp)
    def __rmul__(self, scolar:int):
        return self * scolar
class SquareMatrix(Matrix):
    def __pow__(self, n):
        temp_list = np.array(self.matr_list,)
        res = np.identity(temp_list.shape[0], dtype=int)
        if (n > 0):
            while n > 0:
                if n % 2 == 0:
                    temp_list = matmultiply(temp_list, temp_list)
                    n = n / 2
                else:
                    res = matmultiply(res, temp_list)
                    n -= 1
        return Matrix(res.tolist())
if __name__ == "__main__":
    m = SquareMatrix([[1, 2], [3, 1]])
    print(m ** 3)