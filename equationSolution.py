#!/usr/bin/env python3
x = 0
try:
    a, b, c, d = int(input()), int(input()), int(input()), int(input())
    if (a + b + c + d == 0):
        print("NO")
    else:
        if a == 0 and b ==0:
            print("INF")
        elif b * c == a * d:
            print("NO")
        elif a == 0:
            print("NO")
        elif b % a == 0:
            x = -b // a
            print(x)
        elif b // a == d // c:
            print("NO")
        else:
            print("NO")
except ValueError:
        print('Пожалуйста введите целое число')