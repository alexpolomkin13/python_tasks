#!/usr/bin/env python3
from functools import reduce
print(reduce(lambda el_prev, el: (el ** 5) * el_prev , map(int, input().split()), 1))