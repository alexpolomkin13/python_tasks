#!/usr/bin/env python3
try:
    positions_list = []
    is_probably = False
    for x in range (0, 8):
        str_list = input("Введите координаты ферзя: ").split()
        str_list = [int(item) for item in str_list]
        positions_list.append(str_list)
    for j in range(0, len(positions_list)):
        for k in range(j + 1, len(positions_list)):
            on_dig = abs(positions_list[j][0] - positions_list[k][0]) \
                == abs(positions_list[j][1] - positions_list[k][1])
            on_x = positions_list[j][0] == positions_list[k][0]
            on_y = positions_list[j][1] == positions_list[k][1]
            if (on_dig or on_x or on_y):
                is_probably = True
                break
    print("YES" if is_probably else "NO")
except ValueError as e:
    print(e)