#!/usr/bin/env python3
class Matrix:
    def __init__(self, matr:list):
        self.matr_list = matr
    def __str__(self):
        return (('\n'.join(map(lambda x: ('\t'.join(map(str, x))).strip(),\
            self.matr_list))).strip())
    def size(self):
        return (len(self.matr_list), len(self.matr_list[0]))
    def __add__(self, add_matr):
        temp = list()
        for i in range(0, len(self.matr_list)):
            temp.append([sum(z) for z in zip(self.matr_list[i], add_matr.matr_list[i])])
        return Matrix(temp)
    def __mul__(self, scolar:int):
        temp = list()
        for i in range(0, len(self.matr_list)):
            temp.append(map(lambda x: x * scolar, self.matr_list[i]))
        return Matrix(temp)
    def __rmul__(self, scolar:int):
        return self * scolar
if __name__ == "__main__":
    m1 = Matrix([[1, 0, 0], [0, 1, 0], [0, 0, 1]])
    m2 = Matrix([[0, 1, 0], [20, 0, -1], [-1, -2, 0]])
    print(m1 + m2)