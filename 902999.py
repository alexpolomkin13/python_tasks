#!/usr/bin/env python3
import re
str_i = input()
res = []
for i in str_i.split(','):
    first_dom_level = re.findall(r'\w+.(\w+)$', i)[0]
    res.append(first_dom_level)
    sec_dom_level = re.findall(r'(\w+.\w+)$', i)[0]
    res.append(sec_dom_level)
print('\n'.join(res))