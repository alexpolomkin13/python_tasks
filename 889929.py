#!/usr/bin/env python3
try:
    str_numbers = input("Введите строку чисел: ")
    str_list = str_numbers.split()
    index = 0
    index_max = 0
    max_num = 0
    result = ""
    for num in str_list:
        num = int(num)
        if (max_num == 0 or num >= max_num):
            max_num = num
            index_max = index
        index += 1
    result += str(max_num) + " " + str(index_max)
    print(result)
except ValueError as e:
    print(e)